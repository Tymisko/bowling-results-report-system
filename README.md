# Bowling Results Report System
This is a simple system to calculate the results of a bowling game.

## Requirements
- Node.js
- Angular 15

## Usage
- Clone the repository
- Run `npm install` to install the dependencies
- `npm start` or `ng serve` to start the server
- Choose a file with the player's points. The file must be a `.txt` file.
- The system will show the results of the game in the table.

## Sample correct file
```txt
Paweł Kowalski
1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2
Krzysztof Król
9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9
Izabela Kania
4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4
```

## Showcase

### No File Selected
![Bowling Results Report System - No File Selected Screenshot](./GitLab/bowling-results-no-file-selected.jpg)

### Proper File Selected
![Bowling Results Report System - Proper File Selected Screenshot](./GitLab/bowling-results-proper-file-selected.jpg)

### Incorrect File Selected
![Bowling Results Report System - Incorrect File Selected Screenshot](./GitLab/bowling-results-incorrect-file-selected-sample-error-dialog.jpg)
