import {BowlingJudge} from "../../utils/bowling-judge";
import {IRound} from "../../interfaces/round.interface";

describe('BowlingJudge', () => {
  it('should calculate proper result for player points without strike and spare', () => {
    // Arrange
    let points: number[] = [1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2];

    // Act
    let result: number = BowlingJudge.getPlayerResult(points);

    // Assert
    expect(result).toEqual(30);
  });

  it('should calculate proper result for player points with spare', () => {
    // Arrange
    let points: number[] = [1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 9];

    // Act
    let result: number = BowlingJudge.getPlayerResult(points);

    // Assert
    expect(points.length).toEqual(21);
    expect(result).toEqual(118);
  });

  it('should calculate proper result for player points with strikes', () => {
    // Arrange
    let points: number[] = [1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0, 10, 10];

    // Act
    let result: number = BowlingJudge.getPlayerResult(points);

    // Assert
    expect(result).toEqual(75);
  });

  it('should IRound objects array converted from points have correct length ', () => {
    // Arrange
    let points: number[] = [10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 10];

    // Act
    let rounds: IRound[] = BowlingJudge.convertToRounds(points);

    // Assert
    expect(rounds.length).toEqual(10);
  });

  it('IRound instance should have correct values when it`s last round and STRIKE occurred', () => {
    // Arrange
    let points: number[] = [10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 10];

    // Act
    let lastRound: IRound = BowlingJudge.convertToRounds(points)[9];

    // Assert
    expect(lastRound.firstThrowPoints).toEqual(10);
    expect(lastRound.secondThrowPoints).toBeUndefined();
    expect(lastRound.firstNextRoundThrowPoints).toBeDefined();
    expect(lastRound.secondNextRoundThrowPoints).toBeDefined();
    expect(lastRound.strikeOccurred).toBeTrue();
    expect(lastRound.spareOccurred).toBeFalse();
  });

  it('IRound instance should have correct values when it`s last round and SPARE occured', () => {
    // Arrange
    let points: number[] = [10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 4, 6, 5];

    // Act
    let lastRound: IRound = BowlingJudge.convertToRounds(points)[9];
    let lastRoundPoints: number = lastRound.firstThrowPoints + lastRound.secondThrowPoints!;

    // Assert
    expect(lastRound.spareOccurred).toBeTrue();
    expect(lastRound.strikeOccurred).toBeFalse();
    expect(lastRoundPoints).toEqual(10);
    expect(lastRound.firstThrowPoints).toBeDefined();
    expect(lastRound.secondThrowPoints).toBeDefined();
    expect(lastRound.firstNextRoundThrowPoints).toBeDefined();
    expect(lastRound.secondNextRoundThrowPoints).toBeUndefined();
  });

  it('IRound instances should have correct values', () => {
    // Arrange
    let points: number[] = [10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 4, 3];
    // Act
    let rounds: IRound[] = BowlingJudge.convertToRounds(points);

    // Assert
    expect(rounds.length).toEqual(10);
    for(let round of rounds) {
      expect(round.firstThrowPoints).toBeDefined();
      expect(round.spareOccurred).toBeDefined();
      expect(round.strikeOccurred).toBeDefined();
      expect(round.firstThrowPoints).toBeGreaterThanOrEqual(0);
      expect(round.firstThrowPoints).toBeLessThanOrEqual(10);
    }
  });
});
