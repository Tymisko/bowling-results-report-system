import {FileContentValidator} from "../../utils/file-content-validator";
import {IncorrectPointsLineError} from "../../errors/incorrect-points-line.error";
import {IncorrectNameLineError} from "../../errors/incorrect-name-line.error";

describe('FileContentValidator', () => {
  it('should throw error when points line does not start with a digit', () => {
    let fileContent = 'Paweł Kowalski\n, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2';

    try {
      FileContentValidator.validate(fileContent);
      expect(() => FileContentValidator.validate(fileContent)).toThrowError();
    } catch (error: any) {
      expect(error).toBeInstanceOf(IncorrectPointsLineError);
      expect(error.message).toBe('Error at line 2: Points line must start and end with number');
    }
  });

  it('should throw error when points line does not end with a digit', () => {
    let fileContent = 'Paweł Kowalski\n1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2,';

    try {
      FileContentValidator.validate(fileContent);
      expect(() => FileContentValidator.validate(fileContent)).toThrowError();
    } catch (error: any) {
      expect(error).toBeInstanceOf(IncorrectPointsLineError);
      expect(error.message).toBe('Error at line 2: Points line must start and end with number');
    }
  });

  it('should throw error when points line does not contain only numbers and commas', () => {
    let fileContent = 'Paweł Kowalski\n1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, a, 2';

    try {
      FileContentValidator.validate(fileContent);
      expect(() => FileContentValidator.validate(fileContent)).toThrowError();
    } catch (error: any) {
      expect(error).toBeInstanceOf(IncorrectPointsLineError);
      expect(error.message).toBe('Error at line 2: Points line must be convertable to array of numbers');
    }
  });

  it('should throw error when points line contain array of length less than 20', () => {
    let fileContent = 'Paweł Kowalski\n1, 2, 3';

    try {
      FileContentValidator.validate(fileContent);
      expect(() => FileContentValidator.validate(fileContent)).toThrowError();
    } catch (error: any) {
      expect(error).toBeInstanceOf(IncorrectPointsLineError);
      expect(error.message).toBe('Error at line 2: Points line must contain array of length at least 20 and at most 22');
    }
  });

  it('should throw error when points line contain array of length greater than 22', () => {
    let fileContent = 'Paweł Kowalski\n1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0, 10, 0, 10';
    let points = fileContent.split('\n')[1].split(',').map(p => parseInt(p));

    expect(points.length).toBeGreaterThan(22);

    try {
      FileContentValidator.validate(fileContent);
      expect(() => FileContentValidator.validate(fileContent)).toThrowError();
    } catch (error: any) {
      expect(error).toBeInstanceOf(IncorrectPointsLineError);
      expect(error.message).toBe('Error at line 2: Points line must contain array of length at least 20 and at most 22')
    }
  });

  it('should throw error when points line contain number smaller than 0', () => {
    let fileContent = 'Paweł Kowalski\n1, 0, -2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0';
    let points = fileContent.split('\n')[1].split(',').map(p => parseInt(p));

    expect(points.some(p => p < 0)).toBeTrue();
    try {
      FileContentValidator.validate(fileContent);
      expect(() => FileContentValidator.validate(fileContent)).toThrowError();
    } catch (error: any) {
      expect(error).toBeInstanceOf(IncorrectPointsLineError);
      expect(error.message).toBe('Error at line 2: Points line must contain only numbers between 0 and 10');
    }
  });

  it('should throw error when points line contain number greater than 10', () => {
    let fileContent = 'Paweł Kowalski\n1, 0, 12, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0';
    let points = fileContent.split('\n')[1].split(',').map(p => parseInt(p));

    expect(points.some(p => p > 10)).toBeTrue();
    try {
      FileContentValidator.validate(fileContent);
      expect(() => FileContentValidator.validate(fileContent)).toThrowError();
    } catch (error: any) {
      expect(error).toBeInstanceOf(IncorrectPointsLineError);
      expect(error.message).toBe('Error at line 2: Points line must contain only numbers between 0 and 10');
    }
  });

  it('should throw error when points line contains array of length 21 and last round is not SPARE', () => {
    let fileContent = 'Paweł Kowalski\n1, 0, 10, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 3, 3, 10';
    let points = fileContent.split('\n')[1].split(',').map(p => parseInt(p));

    expect(points[18] + points[19]).not.toBe(10);
    expect(points.length).toBe(21);

    try {
      FileContentValidator.validate(fileContent);
      expect(() => FileContentValidator.validate(fileContent)).toThrowError();
    } catch (error: any) {
      expect(error).toBeInstanceOf(IncorrectPointsLineError);
      expect(error.message).toBe('Error at line 2: To have 21 throws last round must be a spare!');
    }
  });

  it('should throw error when points line contains array of length 22 and last round is not STRIKE', () => {
    let fileContent = 'Paweł Kowalski\n1, 0, 10, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 3, 3, 10, 10';
    let points = fileContent.split('\n')[1].split(',').map(p => parseInt(p));

    expect(points[18] + points[19]).not.toBe(10);
    expect(points.length).toBe(22);

    try {
      FileContentValidator.validate(fileContent);
      expect(() => FileContentValidator.validate(fileContent)).toThrowError();
    } catch (error: any) {
      expect(error).toBeInstanceOf(IncorrectPointsLineError);
      expect(error.message).toBe('Error at line 2: To have 22 throws last round must be a strike!');
    }
  });

  it('should not throw error when file content is correct', () => {
    let fileContent = 'Paweł Kowalski\n' +
      '1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2\n' +
      'Krzysztof Król\n' +
      '9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9\n' +
      'Izabela Kania\n' +
      '4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4\n' +
      'Janusz Cebula\n' +
      '1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 9\n' +
      'A B\n' +
      '1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2\n' +
      'C D\n' +
      '9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9\n' +
      'E F\n' +
      '4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4\n' +
      'G H\n' +
      '1, 9, 0, 10, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 9';

    expect(() => FileContentValidator.validate(fileContent)).not.toThrow();
  });

  it('should throw error when name line is empty', () => {
    let fileContent = ' \n4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4\n' +
      'G H\n1, 9, 0, 10, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 9';

    try {
      FileContentValidator.validate(fileContent);
      expect(() => FileContentValidator.validate(fileContent)).toThrowError();
    } catch (error: any) {
      expect(error).toBeInstanceOf(IncorrectNameLineError);
      expect(error.message).toBe('Error at line 1: Name line cannot be empty');
    }
  });

  it('should not throw error when name line is not empty', () => {
    let fileContent = 'Paweł Kowalski\n1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0';

    expect(() => FileContentValidator.validate(fileContent)).not.toThrow();
  });

  it('should throw error when name line does not contain full name', () => {
    let fileContent = 'Paweł\n1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0';

    try {
      FileContentValidator.validate(fileContent);
      expect(() => FileContentValidator.validate(fileContent)).toThrowError();
    }
    catch (error: any) {
      expect(error).toBeInstanceOf(IncorrectNameLineError);
      expect(error.message).toBe('Error at line: 1: Name line must contain only first and last name separated by space');
    }
  });
});
