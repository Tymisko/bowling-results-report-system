import {TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {FileInputComponent} from "./components/files/file-input.component/file-input.component";
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";
import {ResultsTableComponent} from "./components/results/results-table.component/results-table.component";
import {MatToolbar, MatToolbarModule} from "@angular/material/toolbar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatPaginatorModule} from "@angular/material/paginator";
import {RangePipe} from "./pipes/range.pipe";
import {FooterComponent} from "./components/footer/footer.component";
import {HeaderComponent} from "./components/header/header.component";

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        FileInputComponent,
        ResultsTableComponent,
        FooterComponent,
        HeaderComponent,
        RangePipe
      ],
      imports: [
        MatButtonModule,
        MatDialogModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatPaginatorModule
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Bowling Results Report System'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('Bowling Results Report System');
  });
});
