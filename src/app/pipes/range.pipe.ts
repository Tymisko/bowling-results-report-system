import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'range'
})
export class RangePipe implements PipeTransform{
  transform(_input: any, start: number, end: number, step: number = 1): number[] {
    const result = [];
    for(let i = start; i < end; i += step)
    {
      result.push(i);
    }

    return result;
  }

}
