export interface IRound {
  firstThrowPoints: number;
  secondThrowPoints?: number;
  strikeOccurred: boolean;
  spareOccurred: boolean;
  firstNextRoundThrowPoints?: number;
  secondNextRoundThrowPoints?: number;
}
