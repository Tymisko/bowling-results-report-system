export interface IPlayer {
  firstName: string;
  lastName: string;
  points: number[];
  result: number;
}
