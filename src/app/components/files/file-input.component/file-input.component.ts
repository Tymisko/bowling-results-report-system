import {Component, EventEmitter, Output} from "@angular/core";
import {DialogService} from "../../../services/dialog-service";

@Component({
  selector: 'rs-file-input',
  templateUrl: './file-input.component.html',
  styleUrls: ['./file-input.component.css']
})
export class FileInputComponent {

  constructor(private dialogService: DialogService) {
  }

  @Output() fileSelected: EventEmitter<File> = new EventEmitter<File>();

  selectedFileName: string = 'No file selected';
  private _selectedFile!: File;
  set selectedFile(file: File) {
    this._selectedFile = file;
    this.selectedFileName = file.name;
  }

  get selectedFile(): File {
    return this._selectedFile;
  }

  onFileSelected(event: Event) {
    const target = event.target as HTMLInputElement;
    if (target.files != null && target.files[0].name.endsWith('.txt')) {
      this.selectedFile = target.files[0];
      this.fileSelected.emit(this._selectedFile);
    } else {
      this.dialogService.openOkDialog('Incorrect file selected', 'Please select a .txt file');
    }
  }
}
