import {Component, Input} from "@angular/core";

@Component({
  selector: 'rs-header',
  templateUrl: './header.component.html',
})

export class HeaderComponent {
  @Input() title: string = 'Title';
}
