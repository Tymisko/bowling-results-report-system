import {Component, Input, OnChanges, SimpleChanges} from "@angular/core";
import {IPlayer} from "../../../interfaces/player.interface";
import {PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'rs-results-table',
  templateUrl: './results-table.component.html',
})
export class ResultsTableComponent implements OnChanges {
  @Input() players: IPlayer[] = [];
  filteredPlayers: IPlayer[] = [];
  displayedColumns: string[] = [
    'fullName',
    'result',
    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22'
  ];
  private _listFilter: string = '';
  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredPlayers = this.performFilter(value);
  }

  get listFilter(): string {
    return this._listFilter;
  }

  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: this.pageSize,
    length: this.filteredPlayers.length
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['players']) {
      this.players = changes['players'].currentValue;
      this.filteredPlayers = this.players;
    }
  }

  private performFilter(filterBy: string) : IPlayer[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.players.filter((player: IPlayer) => `${player.firstName} ${player.lastName}`.toLocaleLowerCase().includes(filterBy));
  }
}
