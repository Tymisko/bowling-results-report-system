import {Component} from '@angular/core';
import {FilesHelper} from "./utils/files/files-helper";
import {IPlayer} from "./interfaces/player.interface";
import {BowlingJudge} from "./utils/bowling-judge";
import {FilesContentExtractor} from "./utils/files/files-content-extractor";
import {FileContentValidator} from "./utils/file-content-validator";
import {DialogService} from "./services/dialog-service";

@Component({
  selector: 'rs-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private dialogService: DialogService) {
  }

  title = 'Bowling Results Report System';

  players: IPlayer[] = [];

  onFileSelected(file: File) {
    FilesHelper.getFileContentAsText(file).then(data => {

      try {
        FileContentValidator.validate(data);
      } catch (error: any) {
        this.dialogService.openOkDialog('Incorrect file content', error.message);
        return;
      }

      let playersPoints = FilesContentExtractor.extractPlayerPoints(data);

      this.players = [];
      for (const key of Object.keys(playersPoints)) {
        let fullName = key.split(' ');
        let newPlayer: IPlayer = {
          firstName: fullName[0],
          lastName: fullName[1],
          points: playersPoints[key],
          result: BowlingJudge.getPlayerResult(playersPoints[key])
        };
        this.players.push(newPlayer);
      }
    });
  }
}
