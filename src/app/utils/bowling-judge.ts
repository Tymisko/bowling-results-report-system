import {IRound} from "../interfaces/round.interface";

export class BowlingJudge {
  static getPlayerResult(points: number[]): number {
    let rounds: IRound[] = BowlingJudge.convertToRounds(points);

    let result: number = 0;
    for(let round of rounds)
    {
      result += round.firstThrowPoints
        + (round.secondThrowPoints || 0)
        + (round.firstNextRoundThrowPoints || 0)
        + (round.secondNextRoundThrowPoints || 0);
    }

    return result;
  }

  static convertToRounds(points: number[]): IRound[] {
    let rounds: IRound[] = [];

    let i = 0;
    while(i < 20)
    {
      let round: IRound;

      if (points[i] == 10) {
        round = {
          strikeOccurred: true,
          spareOccurred: false,
          firstThrowPoints: points[i],
          firstNextRoundThrowPoints: points[i + 2],
          secondNextRoundThrowPoints: points[i + 3]
        }
      } else if (points[i] + points[i + 1] == 10) {
        round = {
          strikeOccurred: false,
          spareOccurred: true,
          firstThrowPoints: points[i],
          secondThrowPoints: points[i + 1],
          firstNextRoundThrowPoints: points[i + 2]
        }
      } else {
        round = {
          strikeOccurred: false,
          spareOccurred: false,
          firstThrowPoints: points[i],
          secondThrowPoints: points[i + 1],
        }
      }

      rounds.push(round);
      i+=2;
    }

    return rounds;
  }
}
