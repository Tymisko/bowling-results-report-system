export class FilesContentExtractor {
  /**
   * @param fileContent - the content of the file that follows the structure:
   * <br>firstName lastName
   * <br>0, 1, 2
   */
  static extractPlayerPoints(fileContent: string) {
    const lines: string[] = fileContent.split('\r\n');
    const playersPoints: { [key: string]: number[] } = {};

    for (let i = 0; i < lines.length; i += 2) {
      let nameLine: string = lines[i];
      let pointsLine: string = lines[i + 1];

      playersPoints[nameLine.trim()] = pointsLine
        .replace(' ', '')
        .split(',')
        .map(x => parseInt(x));
    }

    return playersPoints;
  }
}
