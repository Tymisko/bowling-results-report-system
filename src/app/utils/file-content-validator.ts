import {IncorrectPointsLineError} from "../errors/incorrect-points-line.error";
import {IncorrectNameLineError} from "../errors/incorrect-name-line.error";

export class FileContentValidator {
  /**
   * Validates file content and throws error if content is incorrect
   * throws IncorrectPointsLineError if points line is incorrect
   * throws IncorrectNameLineError if name line is incorrect
   */
  static validate(fileContent: string) {
    const lines: string[] = fileContent.split('\n');
    const namesLines: string[] = [];
    const pointsLines: string[] = [];
    for (let i = 0; i < lines.length; i++) {
      lines[i] = lines[i].trim();
      lines[i] = lines[i].replace('\n', '');
      if (i % 2 !== 0) {
        lines[i] = lines[i].replace(' ', '');
        pointsLines.push(lines[i]);
      } else {
        namesLines.push(lines[i]);
      }
    }

    FileContentValidator.validateNamesLines(namesLines);
    FileContentValidator.validatePointsLines(pointsLines);
  }

  private static validatePointsLines(pointsLines: string[]) {
    let lineNumber: number = 2;
    for (let line of pointsLines) {
      let lineIndex : number = pointsLines.indexOf(line);
      lineNumber = lineIndex > 0 ? 2*lineIndex + 2 : 2;

      FileContentValidator.checkIfLineStartsAndEndsWithNumber(line, lineNumber);

      let points: string[] = line.split(',');
      FileContentValidator.checkIfLineIsConvertableToNumbersArray(points, lineNumber);

      let numbers: number[] = points.map(p => parseInt(p));
      FileContentValidator.validateLastRoundSpareOrStrike(points, numbers, lineNumber);
      FileContentValidator.checkIfPointsHaveCorrectValues(numbers, lineNumber);
      FileContentValidator.checkIfPointsArrayHasCorrectLength(numbers, lineNumber);
    }
  }

  private static checkIfPointsArrayHasCorrectLength(numbers: number[], lineNumber: number) {
    if (numbers.length < 20 || numbers.length > 22) {
      throw new IncorrectPointsLineError(`Error at line ${lineNumber}: Points line must contain array of length at least 20 and at most 22`);
    }
  }

  private static validateLastRoundSpareOrStrike(points: string[], numbers: number[], lineNumber: number) {
    switch (points.length) {
      case 21:
        if (numbers[18] + numbers[19] !== 10) {
          throw new IncorrectPointsLineError(`Error at line ${lineNumber}: To have 21 throws last round must be a spare!`);
        }
        break;
      case 22:
        if (numbers[18] !== 10) {
          throw new IncorrectPointsLineError(`Error at line ${lineNumber}: To have 22 throws last round must be a strike!`);
        }
        break;
    }
  }

  private static checkIfPointsHaveCorrectValues(numbers: number[], lineNumber: number) {
    if (numbers.some(n => n < 0 || n > 10)) {
      throw new IncorrectPointsLineError(`Error at line ${lineNumber}: Points line must contain only numbers between 0 and 10`);
    }
  }

  private static checkIfLineIsConvertableToNumbersArray(points: string[], lineNumber: number) {
    if (points.some(p => isNaN(parseInt(p)))) {
      throw new IncorrectPointsLineError(`Error at line ${lineNumber}: Points line must be convertable to array of numbers`);
    }
  }

  private static checkIfLineStartsAndEndsWithNumber(line: string, lineNumber: number) {
    let firstChar: string = line[0];
    let lastChar: string = line[line.length - 1];
    if (isNaN(parseInt(firstChar)) || isNaN(parseInt(lastChar))) {
      throw new IncorrectPointsLineError(`Error at line ${lineNumber}: Points line must start and end with number`);
    }
  }

  private static validateNamesLines(namesLines: string[]) {
    let lineNumber: number = 1;
    for (let line of namesLines) {
      let lineIndex = namesLines.indexOf(line);
      lineNumber = lineIndex > 0 ? lineIndex*2 + 1 : 1;
      FileContentValidator.checkIfLineIsEmpty(line, lineNumber);
      FileContentValidator.checkIfLineHasOnlyTwoWords(line, lineNumber);
    }
  }

  private static checkIfLineIsEmpty(line: string, lineNumber: number) {
    if (line.length === 0) {
      throw new IncorrectNameLineError(`Error at line ${lineNumber}: Name line cannot be empty`);
    }
  }

  private static checkIfLineHasOnlyTwoWords(line: string, lineNumber: number) {
    if (line.split(' ').length !== 2) {
      throw new IncorrectNameLineError(`Error at line: ${lineNumber}: Name line must contain only first and last name separated by space`);
    }
  }
}
