import {MatDialog} from "@angular/material/dialog";
import {OkDialogComponent} from "../components/dialogs/ok-dialog.component/ok-dialog.component";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  constructor(private dialog: MatDialog) {
  }

  openOkDialog(header: string, message: string): void {
    this.dialog.open(OkDialogComponent, {
      width: '250px',
      data: {header: header, content: message}
    });
  }

}
