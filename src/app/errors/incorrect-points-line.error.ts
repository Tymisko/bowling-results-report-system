export class IncorrectPointsLineError extends Error {
  constructor(message: string) {
    super(message);
    this.name = 'IncorrectPointsLineError';
  }
}
