export class IncorrectNameLineError extends Error {
  constructor(message: string) {
    super(message);
    this.name = 'IncorrectNameLineError';
  }
}
