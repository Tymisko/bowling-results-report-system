import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {FormsModule} from "@angular/forms";
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";

// Components
import {OkDialogComponent} from "./components/dialogs/ok-dialog.component/ok-dialog.component";
import {FileInputComponent} from "./components/files/file-input.component/file-input.component";
import {ResultsTableComponent} from "./components/results/results-table.component/results-table.component";
import {MatTableModule} from "@angular/material/table";
import {RangePipe} from "./pipes/range.pipe";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatInputModule} from "@angular/material/input";
import {FooterComponent} from "./components/footer/footer.component";
import {HeaderComponent} from "./components/header/header.component";

@NgModule({
  declarations: [
    AppComponent,
    OkDialogComponent,
    FileInputComponent,
    ResultsTableComponent,
    FooterComponent,
    HeaderComponent,
    RangePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NoopAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatInputModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
